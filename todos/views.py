from django.shortcuts import render, redirect
from .models import TodoList


# Create your views here.
# Can view the entire todo list
def todo_list_list(request):
    todo_list = TodoList.objects.all()
    context = {
        "todo_list": todo_list,
    }
    return render(request, "todos/todo_list_list.html", context)


# Shows details of a particular to-do list, including tasks
def todo_list_detail(request, id):
    todo_list = TodoList.objects.get(id=id)
    context = {
        "task_detail": todo_list
    }
    return render(request, "todos/todo_list_details.html", context)
